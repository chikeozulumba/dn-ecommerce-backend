import { Router } from 'express'
import checkAuth from '../../middlewares/checkAuth'
import { validateOrderId } from '../order/order.validator'
import asyncHandler from '../../middlewares/asyncHandler'
import Receipts from './receipt.controller'
import { checkIfInvoicePaid } from './middlewares/receipt.middleware'

const router = Router()

router
  .route('/')
  .all(asyncHandler(checkAuth))
  .get(asyncHandler(Receipts.getMany))

router
  .route('/:id')
  .all(
    asyncHandler(checkAuth),
    validateOrderId,
    asyncHandler(Receipts.checkRecord),
    asyncHandler(checkIfInvoicePaid),
    asyncHandler(Receipts.checkOwnership),
  )
  .get(asyncHandler(Receipts.getOne))

router
  .route('/download/:id')
  .all(
    asyncHandler(checkAuth),
    validateOrderId,
    asyncHandler(Receipts.checkRecord),
    asyncHandler(checkIfInvoicePaid),
    asyncHandler(Receipts.checkOwnership),
  )
  .get(asyncHandler(Receipts.downloadReceipt))

export default router
